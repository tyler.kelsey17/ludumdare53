# Ludum Dare 51

This is our game for the 51th [LDJam](https://ldjam.com)

## Repo Setup
Unity Version: 2021.3.23f1


Enable large file system!

```
git lfs install
```

## Play the game

Latest Build: https://fachschafttf.gitlab.io/ludumdare53/

